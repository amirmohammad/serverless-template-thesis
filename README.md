
# Function Templates

Hey, I'm currently working on my thesis at Shahid Beheshti University Of Tehran. My Thesis is about mitigating serverless coldstart overhead using MicroVMs. This repo represents function templates to deploy in my serverless platform. 

The chosen temporal name for my platform is Cloudsec Worker.

Cloudsec worker currently supports Javascript functions. Other runtimes will be added overtime, including Python, Java, Go, etc.

#

## Javascript
Fork the project. Go to Nodejs directory and write your function in node*/function. Install the required dependencies in the main route (node*).
For further Information, see comments in code.



# cautious

This repo is a fork from openfaas template repo. (see https://github.com/openfaas/templates )