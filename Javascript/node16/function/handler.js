'use strict'
// Write your functions here.
// You can import 3rd party packages here. Install the dependencies in the parent directory.

module.exports = async (event, context) => {
  
  const result = {
    'body': JSON.stringify(event.body),
    'content-type': event.headers["content-type"]
  }

  return context
    .status(200)
    .succeed(result)
}
